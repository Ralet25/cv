using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovements : MonoBehaviour
{
    [SerializeField] private float Speed;
    [SerializeField] private float Jump_Force;
    [SerializeField] private float gravityForce;

    private float gravitymove;

    private float velx;
    private float velz;

    private Vector3 InputdeMove;

    CharacterController control;

    void Start()
    {
        control = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        Jump();
    }
    void Jump()
    {
        if (Input.GetKey(KeyCode.Space)) control.Move(transform.up * 1 * Jump_Force / 50);

        if (!control.isGrounded)
        {
            gravitymove -= gravityForce / 1000;
        }
        else
        {
            gravitymove = 0;
        }

    }

    void Movement()
    {
        velx = Input.GetAxisRaw("Horizontal");
        velz = Input.GetAxisRaw("Vertical");
        InputdeMove = transform.right * velx / 10  + transform.forward * velz * Speed / 10 ;

        control.Move(InputdeMove);
        control.Move(new Vector3(0, gravitymove, 0));
    }

}