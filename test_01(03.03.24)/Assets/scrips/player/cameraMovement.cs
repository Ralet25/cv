using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraMovement : MonoBehaviour
{
    private float xRotation;    // Variables para controlar las rotaciones en los ejes X
    private float yRotation;    // Variables para controlar las rotaciones en los ejes y
    public float sensX;     // Sensibilidad del movimiento en el eje X 
    public float sensY;     // Sensibilidad del movimiento en el eje y
    public Transform orientation;  // Referencia al objeto de orientacion

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxisRaw("Mouse X") * Time.deltaTime * sensX;
        float mouseY = Input.GetAxisRaw("Mouse Y") * Time.deltaTime * sensY;
        yRotation += mouseX;         // Actualiza la rotaci�n en el eje y con el movimiento del raton
        xRotation -= mouseY;         // Actualiza la rotaci�n en el eje x con el movimiento del raton
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.rotation = Quaternion.Euler(xRotation, yRotation, 0);      // Aplica la rotacion a la camara en funci�n de las rotaciones en los ejes X e Y
        orientation.rotation = Quaternion.Euler(0, yRotation, 0);        // Aplica la rotacion a la orientacion en el eje Y

        // Captura la entrada del raton para el movimiento de la c�mara
    }
}
