using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WeaponControler : MonoBehaviour
{
    public List<GameObject> FirePoints;
    public GameObject Bullet;
    public WeaponData weapon;

    public TextMeshProUGUI Munition;

    float ShootLoad;
    float ReloadLoad;
    float ActualMunition;

    private void Start()
    {
        ShootLoad = weapon.ShootLoad;
        ActualMunition = weapon.MaxArmo;
        ReloadLoad = weapon.LoadSpeed;
    }

    void Update()
    {
        Munition.text = ActualMunition + " / " + weapon.MaxArmo;

        if (Input.GetMouseButton(0) && ShootLoad < Time.time && ActualMunition > 0)
        {
            shoot();
        }
        if (Input.GetKey(KeyCode.R) && ReloadLoad < Time.time)
        {
            reload();
        }
    }

    public void shoot() 
    {
        for(int i = 0; i < FirePoints.Count; i++) 
        {
            GameObject shoot = Instantiate(Bullet, FirePoints[i].transform.position, new Quaternion(0, 0, 0, 0));
            shoot.GetComponent<Rigidbody>().AddForce(transform.forward * weapon.BulletVelocity);
            Destroy(shoot, 5f);
            ShootLoad = Time.time + weapon.ShootLoad;
            ActualMunition -= 1;
        }
    }

    public void reload() 
    {
        ActualMunition = weapon.MaxArmo;
        ReloadLoad = Time.time + weapon.LoadSpeed;
    }
}
