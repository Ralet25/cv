using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Data", menuName = "Weapon", order = 1)]
public class WeaponData : ScriptableObject
{
    [Header("Weapon")]
    public int Firepoints; //cantidad de los puntos de donde sale el disparo
    public int MaxArmo; //cantidad maxima de municion
    public int WeaponDamage; //cantidad de da�o
    public int BulletVelocity; //velocidad del disparo
    public float ShootLoad; //tiempo entre cada disparo
    public float LoadSpeed; //tiempo de recarga
    public ParticleSystem Particles; //particulas de explocion del arma
}
