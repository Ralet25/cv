using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageControler : MonoBehaviour
{
    public int Life;
    protected GameObject Weapon;

    private void Start()
    {
        Weapon = GameObject.Find("Weapon");
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerBullet")
        {
            other.gameObject.GetComponent<BulletEvent>().Destruction();
            Life -= Weapon.GetComponent<WeaponControler>().weapon.WeaponDamage;

            if (Life <= 0)
            {
                Destroy(gameObject);
            }
        }
    }
}
